#!/bin/bash

# LXC
# lxc.cgroup.devices.allow = b 7:* rwm
#mknod /dev/loop5i" -m0660 b 7 5
#mknod /dev/loop6 -m0660 b 7 6
#mknod /dev/loop7 -m0660 b 7 7
#mknod /dev/loop8 -m0660 b 7 8
#mknod /dev/loop9 -m0660 b 7 9
#mknod /dev/loop10 -m0660 b 7 10

mkdir -p /mnt/$1 /mnt/$1/system
umount /mnt/$1/system/vendor /mnt/$1/system/product /mnt/$1/system
mount -o loop /home/gnumdk/android/tmp/"$1"_system.img /mnt/$1/system
mount -o loop /home/gnumdk/android/tmp/"$1"_vendor.img /mnt/$1/system/vendor
mount -o loop /home/gnumdk/android/tmp/"$1"_vendor.img /mnt/$1/system/product
