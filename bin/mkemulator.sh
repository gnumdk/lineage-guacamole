#!/bin/bash

rm -f /data/android/www/x86/*.zip
cd ~/android/lineage
source build/envsetup.sh
lunch lineage_x86-eng
#mka
mka sdk_addon
cd -
mkdir -p /data/android/www/x86
mv ~/android/lineage/out/host/linux-x86/sdk_addon/lineage-eng.gnumdk-linux-x86-img.zip /data/android/www/x86/lineage-19.1-$(date +%Y%m%d%H%M)-nightly-x86-signed.zip
exit $?
