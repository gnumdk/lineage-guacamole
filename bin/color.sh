#!/bin/bash

find . -name *.png | ( while read file
do
	convert $file  -fuzz 20% -fill white +opaque black $file-white.png
	mv -f $file-white.png $file
done
)
