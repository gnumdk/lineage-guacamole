#!/bin/bash
i=461
while ((percent < 100))
do
    convert base.png -fill black -colorize $percent% 00$i.png
    ((percent+=3))
    ((i+=1))
done
