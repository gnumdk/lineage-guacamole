#!/bin/bash

SCRIPT_DIR=/home/gnumdk/android/apks/magisk
export PATH=/home/gnumdk/android/bin:$PATH
export BOOTMODE=true
export KEEPVERITY=true

[[ ! -d $SCRIPT_DIR ]] && exit 0

cp $SCRIPT_DIR/lib/x86/libmagiskboot.so $SCRIPT_DIR/assets/magiskboot
cp $SCRIPT_DIR/lib/arm64-v8a/libmagisk64.so $SCRIPT_DIR/assets/magisk64
cp $SCRIPT_DIR/lib/armeabi-v7a/libmagisk32.so $SCRIPT_DIR/assets/magisk32
cp $SCRIPT_DIR/lib/arm64-v8a/libmagiskinit.so $SCRIPT_DIR/assets/magiskinit

. $SCRIPT_DIR/assets/boot_patch.sh $*
