#!/bin/bash

source ~/android/bin/init.sh $@

repo_path=~/android/www/$RELEASE

JSON=$repo_path/ota.json

JSON_DATA='
    {
      "datetime": @DATETIME@, 
      "filename": "@FILENAME@", 
      "id": "@ID@", 
      "romtype": "UNOFFICIAL",
      "size": @SIZE@, 
      "url": "https://adishatz.org/ota/@REMOTE@/@FILENAME@", 
      "version": "19.1"
    } 
'
first=0
echo '{
  "response": [' > $JSON
for file in $repo_path/*.zip
do
	if ((first!=0))
	then
		echo "," >> $JSON
	fi
	first=1
	filename=$(basename $file)
	size=$(stat --printf="%s" $file)
	date=$(stat --printf="%Y" $file)
        version=$(date +%Y%m%d%H%M)
	id=$(sha256sum $file | cut -d ' ' -f1)
	echo $JSON_DATA|sed "s/@DATETIME@/$date/g;s/@FILENAME@/$filename/g;s/@ID@/$id/g;s/@SIZE@/$size/g;s/@VERSION@/$version/g;s/@REMOTE@/$RELEASE/g" >> $JSON
done

echo ']
}
' >> $JSON
