#!/bin/bash

source ~/android/bin/init.sh $@


function apply_patch
{
    cd ~/android/lineage
    basename="$(basename $1)"
    # Check if patch is wanted
    if [[ $basename == __* ]]
    then
        if [[ $basename != __$RELEASE* ]]
        then
            return 0
        fi
    fi
    dirname="$(dirname $1)"
    md5=$(md5sum ~/android/patches/$1)
    mkdir -p ~/android/tmp/patches/$dirname
    grep $md5  ~/android/tmp/patches/$dirname/$basename.md5 >/dev/null 2>&1 && return 0
    [[ ! -e ~/android/lineage/$dirname || -e ~/android/tmp/patches/$dirname/$basename ]] && return 0
    cd ~/android/lineage/$dirname
	mkdir -p ~/android/tmp/patches/$dirname
    echo $md5 > ~/android/tmp/patches/$dirname/$basename.md5
    patch --no-backup-if-mismatch -f -p1 -i ~/android/patches/"$1"
    (( $? != 0 )) && touch ~/android/tmp/error
}

function patch_dir
{
    cd ~/android/patches
    find "$1" -name *.diff | while read patch
    do
        apply_patch $patch
    done
    cd - >/dev/null
}

rm -f  ~/android/tmp/error
cd ~/android/lineage
source build/envsetup.sh
mkdir -p ~/android/tmp/patches
cd ~/android/patches/
find . -name *.diff | awk -F '/' '{for(i=2; i<NF; i++) printf("%s%s", $i, FS); print ""}' | sort | uniq | ( while read dir
    do	
		echo ">>> $dir"
        patch_dir "$dir"
    done
) 
[[ -e ~/android/tmp/error ]] && exit -1

# NFC CVE
rm -fr ~/android/lineage/cts/hostsidetests/securitybulletin/securityPatch/CVE-2019-2135
rm -fr ~/android/lineage/cts/hostsidetests/securitybulletin/securityPatch/CVE-2021-0956
rm -fr ~/android/lineage/cts/hostsidetests/securitybulletin/securityPatch/CVE-2019-2134
rm -fr ~/android/lineage/cts/hostsidetests/securitybulletin/securityPatch/CVE-2019-2133
rm -fr ~/android/lineage/cts/hostsidetests/securitybulletin/securityPatch/CVE-2021-0596

# Bootsplash
cp -f ~/android/data/bootanimation.tar  ~/android/lineage/vendor/lineage/bootanimation/bootanimation.tar

# Add prebuilt APKS
cd $DEVICE_DIR
git checkout $DEVICE_MK
cd -
if [[ "$RELEASE" == "gts4lvwifi" ]]
then
echo "
PRODUCT_PACKAGES += \
   FDroidPrivilegedExtension \
   AuroraExtension
" >> $DEVICE_DIR/$DEVICE_MK
else
echo "
PRODUCT_PACKAGES += \
   GmsCore \
   GsfProxy \
   DroidGuard \
   FakeStore \
   FDroidPrivilegedExtension \
   AuroraExtension
" >> $DEVICE_DIR/$DEVICE_MK
fi
for package in AuroraExtension
do
    cd ~/android/lineage/packages/apps/$package
    git lfs pull
    cd -
done
