#!/bin/bash

source ~/android/bin/init.sh $@

function get_modules
{
    MANIFESTS="manifest.xml manifests/default.xml manifests/snippets/lineage.xml local_manifests/roomservice.xml"
    cd ~/android/lineage
    for manifest in $MANIFESTS
    do
        grep project ~/android/lineage/.repo/$manifest | sed 's/.*path="\([^"]*\).*/\1/' | ( while read dir
            do
                [[ -d ~/android/lineage/$dir && -d ~/android/patches/$dir ]] && echo $dir
            done
        )
    done
}

# Check orphaned patched files
for module in $(get_modules)
do
    cd ~/android/lineage/$module
    git status -s | awk -F ' ' '{print $2}' | ( while read file
        do
            # Check if file not patched anymore
            grep $file ~/android/patches/$module/*.diff >/dev/null
            if (( $? != 0 ))
            then
                rm -vf $file
                git checkout $file >/dev/null 2>&1
            fi
        done
    )
done

# Check modified patches
cd ~/android/patches
find . -name *.diff | ( while read file
    do
        basename="$(basename $file)"
        # Check if patch is wanted
        if [[ $basename == __* ]]
        then
            if [[ $basename != __$RELEASE* ]]
            then
                continue
            fi
        fi
        dirname="$(dirname $file)"
        [[ ! -f ~/android/tmp/patches/$dirname/$basename.md5 ]] && continue
        md5=$(md5sum ~/android/patches/"$file")
        grep $md5  ~/android/tmp/patches/$dirname/$basename.md5 >/dev/null 2>&1 && continue
        [[ ! -e ~/android/lineage/"$dirname" ]] && continue
        cd ~/android/lineage/"$dirname"
        while read previously
        do
             rm -vf $previously
             git checkout $previously
        done <<< $(grep '+++ ' ~/android/patches/$dirname/$basename | cut -d '/' -f2-)
        rm -f ~/android/patches/$dirname/"${basename%.*}".md5
        cd ~/android/lineage
    done
)

if [[ "$RELEASE" != "x86" ]]
then
    cd $DEVICE_DIR
    pwd
    echo $DEVICE_MK
    git checkout $DEVICE_MK
fi
