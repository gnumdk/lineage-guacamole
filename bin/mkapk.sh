#!/bin/bash

source ~/android/bin/init.sh $@

cd ~/android/lineage
source build/envsetup.sh
if [[ ! -d ~/.android-certs ]]
then
	echo "https://wiki.lineageos.org/signing_builds.html"
	exit 0
fi
breakfast $SUBRELEASE
mka target-files-package otatools
(( $? != 0 )) && exit -1
~/android/bin/magisk.sh ~/android/lineage/out/target/product/$RELEASE/boot.img >/dev/null
mv ~/android/apks/magisk/assets/new-boot.img ~/android/lineage/out/target/product/$RELEASE/boot.img
exit $?
