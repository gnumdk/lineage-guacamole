#!/bin/bash

if (( $# == 0 ))
then
    exit 0
fi

curl https://storage.googleapis.com/git-repo-downloads/repo > ~/android/bin/repo
chmod +x ~/android/bin/repo
source ~/android/bin/init.sh $@

if (( FORCE_CLEAN_KERNEL == 1 ))
then
    rm -fvr ~/android/lineage/out/target/product/$RELEASE/obj/DTBO_OBJ ~/android/lineage/out/target/product/$RELEASE/obj/KERNEL_OBJ  ~/android/lineage/out/target/product/$RELEASE/obj/DTB_OBJ
    exit 0
fi

if (( FORCE_CLEAN == 1 ))
then
    cd ~/android/lineage
    source build/envsetup.sh
    ~/android/bin/repo forall -vc "git reset --hard; git clean -fd"
    ~/android/bin/repo sync --force-remove-dirty --force-sync -v
    make installclean
    rm -rvf ~/android/lineage/out/target/product/$RELEASE
    rm -fr ~/android/tmp/patches
    exit 0
fi

exec > >(tee -i ~/android/buildlog)
exec 2>&1

if [[ ! -d ~/android/lineage/android ]]
then
    ~/android/bin/mkrepo.sh
    FORCE_UPDATE=1
elif (( FORCE_UPDATE == 1 ))
then
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/android/bin/repo
    chmod +x ~/android/bin/repo
    FORCE_UPDATE=1
fi

if (( UPDATE_PATCH == 1 ))
then
    ~/android/bin/clean_patch.sh
    exit 0
fi

if (( BOOT==1 ))
then
	cd ~/android/lineage
	source build/envsetup.sh
    rm -fr  ~/android/lineage/out/target/product/$RELEASE/obj/DTB* ~/android/lineage/out/target/product/$RELEASE/obj/KERNEL_OBJ/*
    breakfast $SUBRELEASE
	make bootimage
    (( $? != 0 )) && exit -1
    ~/android/bin/magisk.sh ~/android/lineage/out/target/product/$RELEASE/boot.img >/dev/null
    mv ~/android/apks/magisk/assets/new-boot.img ~/android/lineage/out/target/product/$RELEASE/boot.img
    exit 0
fi

if (( FORCE_UPDATE==1 ))
then
    cd ~/android/lineage
    cp ~/android/files/roomservice.xml ~/android/lineage/.repo/local_manifests
    ~/android/bin/repo forall -vc "git reset --hard; git clean -fd"
    ~/android/bin/repo sync --force-remove-dirty --force-sync -v
    rm -fr ~/android/lineage/vendor/$VENDOR
    rm -rf ~/android/lineage/out/target/product/$RELEASE
    rm -fr ~/android/tmp/patches
fi

if (( PATCH_FORCE == 1 ))
then
    cd ~/android/lineage
    source build/envsetup.sh
    rm -fr ~/android/tmp/patches
    ~/android/bin/repo forall -vc "git reset --hard; git clean -fd"
    ~/android/bin/mkclean.sh
    ~/android/bin/mkpatches.sh > /tmp/mkpatches
    grep \.rej /tmp/mkpatches
    exit 0
elif (( PATCH_ONLY == 1 ))
then
    ~/android/bin/mkclean.sh
    ~/android/bin/mkpatches.sh
    exit 0
else
    ~/android/bin/mkclean.sh
    ~/android/bin/mkpatches.sh || exit -1
fi

sleep 5

rm -fr  ~/android/lineage/out/target/product/$RELEASE/obj/DTB* ~/android/lineage/out/target/product/$RELEASE/obj/KERNEL_OBJ/*
if [[ $RELEASE != "x86" ]]
then
    if [[ ! -e ~/android/lineage/vendor/$VENDOR/$RELEASE ]]
    then
        if [[ -e ~/android/lineage/vendor/"$VENDOR"_lineage ]]
        then
            cd ~/android/lineage/vendor/
            mkdir -p ~/android/tmp/$RELEASE
            unionfs "$VENDOR"_lineage/$RELEASE:"$VENDOR"_lineage/$COMMON ~/android/tmp/$RELEASE
            cd ~/android/lineage/device/$VENDOR/$RELEASE
            ./extract-files.sh ~/android/tmp/$RELEASE/proprietary
            umount ~/android/tmp/$RELEASE
            rmdir ~/android/tmp/$RELEASE
        else
            cd ~/android/lineage/device/$VENDOR/$RELEASE
            ./extract-files.sh ~/android/lineage/vendor/$RELEASE/proprietary
        fi
    fi
    cd - >/dev/null
    ~/android/bin/mkapk.sh || exit 0
    ~/android/bin/mkzip.sh || exit 0
    ~/android/bin/mkota.sh
else
    ~/android/bin/mkemulator.sh
fi
