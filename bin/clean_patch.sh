#!/bin/bash

source ~/android/bin/init.sh $@

cd ~/android/lineage
source build/envsetup.sh

cd ~/android/patches/
find . -name *.diff | sort | ( while read file
    do
        basename="$(basename $file)"
        # Check if patch is wanted
        if [[ $basename == __* ]]
        then
            if [[ $basename != __$RELEASE* ]]
            then
                continue
            fi
        fi
        dirname="$(dirname $file)"
        [[ ! -e ~/android/lineage/$dirname ]] && continue
        cd ~/android/lineage/$dirname
        git reset --hard; git clean -fd
        echo $file
        patch --no-backup-if-mismatch -p1 -i ~/android/patches/"$file"
        if [[ "$?" != "0" ]]
        then
            echo "Problème avec $file"
            exit -1
        fi
        git add -AN
        git diff --text > ~/android/patches/"$file"
        git reset --hard; git clean -fd
        cd ~/android/lineage
    done	
) || exit -1
