[[ ! -d lineage ]] && exit 0
cd lineage
repo forall -vc "git reset --hard; git clean -fd" 
repo sync --force-remove-dirty --force-sync -v 
cd -
