#!/bin/bash

if (( $# != 0 ))
then
    export PATH=$HOME/android/lineage/prebuilts/clang/host/linux-x86/clang-proton/bin:$HOME/android/lineage/out/soong/host/linux-x86/bin:$PATH
    #export PATH=$PATH:$HOME/android/lineage/out/soong/host/linux-x86/bin
    export RELEASE="guacamole"
    export VENDOR="oneplus"
    export COMMON="sm8150-common"
    export DEVICE_DIR=~/android/lineage/device/$VENDOR/$RELEASE
    export DEVICE_MK=device.mk
    export USE_CCACHE=1
    export CCACHE_EXEC=/usr/bin/ccache
    export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx34G"

	export BOOT=0
    export FORCE_CLEAN_KERNEL=0
    export FORCE_CLEAN=0
    export FORCE_UPDATE=0
    export PATCH_ONLY=0
    export PATCH_FORCE=0
    export UPDATE_PATCH=0

    for arg in $@
    do
        if [[ "$arg" == "tab" ]]
        then
            export RELEASE="gts4lvwifi"
            export VENDOR="samsung"
            export COMMON="gts4lv-common"
            export DEVICE_DIR=~/android/lineage/device/$VENDOR/$RELEASE
		elif [[ "$arg" == "tablte" ]]
		then
            export RELEASE="gts4lv"
            export VENDOR="samsung"
            export COMMON="gts4lv-common"
            export DEVICE_DIR=~/android/lineage/device/$VENDOR/$RELEASE
        elif [[ "$arg" == "pixel" ]]
        then
            export RELEASE="sailfish"
            export VENDOR="google"
            export COMMON="marlin"
            export DEVICE_DIR=~/android/lineage/device/$VENDOR/$COMMON
            export DEVICE_MK=device-$RELEASE.mk
        elif [[ "$arg" == "x86" ]]
        then
            export RELEASE="x86"
            export VENDOR="generic"
            export COMMON=""
            export DEVICE_DIR=~/android/lineage/device/$VENDOR/$RELEASE
            export DEVICE_MK=mini_x86.mk
        fi
        if [[ "$arg" == "clean" ]]
        then
            export FORCE_CLEAN=1
        elif [[ "$arg" == "clean_kernel" ]]
        then
            export FORCE_CLEAN_KERNEL=1
        elif [[ "$arg" == "sync" ]]
        then
            export FORCE_UPDATE=1
        elif [[ "$arg" == "patch" ]]
        then
            export PATCH_ONLY=1
        elif [[ "$arg" == "patch_force" ]]
        then
            export PATCH_FORCE=1
        elif [[ "$arg" == "boot" ]]
        then
            export BOOT=1
        elif [[ "$arg" == "update" ]]
        then
            export UPDATE_PATCH=1
        fi
    done
    export SUBRELEASE=lineage_$RELEASE-user
    export OUT=~/android/lineage/out/target/product/$RELEASE
fi
