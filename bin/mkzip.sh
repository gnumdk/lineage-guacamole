#!/bin/bash

source ~/android/bin/init.sh $@

repo_path=~/android/www/$RELEASE
mkdir -p "$repo_path"
zip_path=$repo_path/lineage-19.1-$(date +%Y%m%d%H%M)-nightly-$RELEASE-signed.zip

zip_files=$(ls $repo_path/*.zip)

cd ~/android/lineage
source build/envsetup.sh
croot
sign_target_files_apks -o -d ~/.android-certs\
    -p out/host/linux-x86\
    $OUT/obj/PACKAGING/target_files_intermediates/*-target_files-*.zip \
    ~/android/tmp/signed-target_files.zip

ota_from_target_files -k ~/.android-certs/releasekey \
    --block -p out/host/linux-x86\
    ~/android/tmp/signed-target_files.zip \
    $zip_path

rm -f $zip_files

if [[ "$?" != "0" ]]
then
    echo "Problème de build" | mail -s "Erreur de build Android" cedric.bellegarde@adishatz.org	
	exit -1
fi
