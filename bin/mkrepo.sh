#!/bin/bash

mkdir -p ~/android/lineage
cd ~/android/lineage
~/android/bin/repo init -u https://github.com/LineageOS/android.git -b lineage-19.1 --depth=10
mkdir -p ~/android/lineage/.repo/local_manifests
