diff --git a/drivers/gpu/msm/kgsl_drawobj.c b/drivers/gpu/msm/kgsl_drawobj.c
index c927fdccb..41dd6dc02 100644
--- a/drivers/gpu/msm/kgsl_drawobj.c
+++ b/drivers/gpu/msm/kgsl_drawobj.c
@@ -43,6 +43,10 @@
 static struct kmem_cache *memobjs_cache;
 static struct kmem_cache *sparseobjs_cache;
 
+static struct kmem_cache *drawobj_sparse_cache;
+static struct kmem_cache *drawobj_sync_cache;
+static struct kmem_cache *drawobj_cmd_cache;
+
 static void free_fence_names(struct kgsl_drawobj_sync *syncobj)
 {
 	unsigned int i;
@@ -68,14 +72,14 @@ void kgsl_drawobj_destroy_object(struct kref *kref)
 		syncobj = SYNCOBJ(drawobj);
 		free_fence_names(syncobj);
 		kfree(syncobj->synclist);
-		kfree(syncobj);
+		kmem_cache_free(drawobj_sync_cache, syncobj);
 		break;
 	case CMDOBJ_TYPE:
 	case MARKEROBJ_TYPE:
-		kfree(CMDOBJ(drawobj));
+		kmem_cache_free(drawobj_cmd_cache, CMDOBJ(drawobj));
 		break;
 	case SPARSEOBJ_TYPE:
-		kfree(SPARSEOBJ(drawobj));
+		kmem_cache_free(drawobj_sparse_cache, SPARSEOBJ(drawobj));
 		break;
 	}
 }
@@ -669,12 +673,27 @@ int kgsl_drawobj_cmd_add_ibdesc(struct kgsl_device *device,
 }
 
 static void *_drawobj_create(struct kgsl_device *device,
-	struct kgsl_context *context, unsigned int size,
-	unsigned int type)
+	struct kgsl_context *context, unsigned int type)
 {
-	void *obj = kzalloc(size, GFP_KERNEL);
+	void *obj;
 	struct kgsl_drawobj *drawobj;
 
+	switch (type) {
+	case SYNCOBJ_TYPE:
+		obj = kmem_cache_zalloc(drawobj_sync_cache, GFP_KERNEL);
+		break;
+	case CMDOBJ_TYPE:
+	case MARKEROBJ_TYPE:
+		obj = kmem_cache_zalloc(drawobj_cmd_cache, GFP_KERNEL);
+		break;
+	case SPARSEOBJ_TYPE:
+		obj = kmem_cache_zalloc(drawobj_sparse_cache, GFP_KERNEL);
+		break;
+	default:
+		// noop
+		return ERR_PTR(-ENOMEM);
+	}
+
 	if (obj == NULL)
 		return ERR_PTR(-ENOMEM);
 
@@ -683,7 +702,18 @@ static void *_drawobj_create(struct kgsl_device *device,
 	 * during the lifetime of this object
 	 */
 	if (!_kgsl_context_get(context)) {
-		kfree(obj);
+		switch (type) {
+		case SYNCOBJ_TYPE:
+			kmem_cache_free(drawobj_sync_cache, obj);
+			break;
+		case CMDOBJ_TYPE:
+		case MARKEROBJ_TYPE:
+			kmem_cache_free(drawobj_cmd_cache, obj);
+			break;
+		case SPARSEOBJ_TYPE:
+			kmem_cache_free(drawobj_sparse_cache, obj);
+			break;
+		}
 		return ERR_PTR(-ENOENT);
 	}
 
@@ -711,7 +741,7 @@ struct kgsl_drawobj_sparse *kgsl_drawobj_sparse_create(
 		struct kgsl_context *context, unsigned int flags)
 {
 	struct kgsl_drawobj_sparse *sparseobj = _drawobj_create(device,
-		context, sizeof(*sparseobj), SPARSEOBJ_TYPE);
+		context, SPARSEOBJ_TYPE);
 
 	if (!IS_ERR(sparseobj))
 		INIT_LIST_HEAD(&sparseobj->sparselist);
@@ -731,7 +761,7 @@ struct kgsl_drawobj_sync *kgsl_drawobj_sync_create(struct kgsl_device *device,
 		struct kgsl_context *context)
 {
 	struct kgsl_drawobj_sync *syncobj = _drawobj_create(device,
-		context, sizeof(*syncobj), SYNCOBJ_TYPE);
+		context, SYNCOBJ_TYPE);
 
 	/* Add a timer to help debug sync deadlocks */
 	if (!IS_ERR(syncobj))
@@ -756,8 +786,7 @@ struct kgsl_drawobj_cmd *kgsl_drawobj_cmd_create(struct kgsl_device *device,
 		unsigned int type)
 {
 	struct kgsl_drawobj_cmd *cmdobj = _drawobj_create(device,
-		context, sizeof(*cmdobj),
-		(type & (CMDOBJ_TYPE | MARKEROBJ_TYPE)));
+		context, (type & (CMDOBJ_TYPE | MARKEROBJ_TYPE)));
 
 	if (!IS_ERR(cmdobj)) {
 		/* sanitize our flags for drawobj's */
@@ -1151,14 +1180,23 @@ void kgsl_drawobjs_cache_exit(void)
 {
 	kmem_cache_destroy(memobjs_cache);
 	kmem_cache_destroy(sparseobjs_cache);
+
+	kmem_cache_destroy(drawobj_sparse_cache);
+	kmem_cache_destroy(drawobj_sync_cache);
+	kmem_cache_destroy(drawobj_cmd_cache);
 }
 
 int kgsl_drawobjs_cache_init(void)
 {
-	memobjs_cache = KMEM_CACHE(kgsl_memobj_node, 0);
-	sparseobjs_cache = KMEM_CACHE(kgsl_sparseobj_node, 0);
+	memobjs_cache = KMEM_CACHE(kgsl_memobj_node, SLAB_HWCACHE_ALIGN);
+	sparseobjs_cache = KMEM_CACHE(kgsl_sparseobj_node, SLAB_HWCACHE_ALIGN);
+
+	drawobj_sparse_cache = KMEM_CACHE(kgsl_drawobj_sparse, SLAB_HWCACHE_ALIGN);
+	drawobj_sync_cache = KMEM_CACHE(kgsl_drawobj_sync, SLAB_HWCACHE_ALIGN);
+	drawobj_cmd_cache = KMEM_CACHE(kgsl_drawobj_cmd, SLAB_HWCACHE_ALIGN);
 
-	if (!memobjs_cache || !sparseobjs_cache)
+	if (!memobjs_cache || !sparseobjs_cache ||
+	    !drawobj_sparse_cache || !drawobj_sync_cache || !drawobj_cmd_cache)
 		return -ENOMEM;
 
 	return 0;
